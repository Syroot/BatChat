@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION
CLS
COLOR 0B
SET ChatTopic=Welcome to BatChat
SET /A ChatUsersOnline=0
CALL "BatChat Config.bat"
SET ChatSuppressBeep=yes

:RereadLog
CLS
SET /A ChatLinesRead=0

:ReadLog
SET ChatHour=%time:~0,2%
SET ChatMin=%time:~3,2%
SET ChatSec=%time:~6,2%

:: Check if the log exists at all
IF /I NOT EXIST "%ChatLog%" GOTO ChatLogNotFound

:: Read log file line per line
SET /A ChatCurrentLine=0
FOR /F "tokens=*" %%a in (%ChatLog%) DO (
    SET ChatLine=%%a
    SET ChatCommand=no
    
    :: Get the current line number
    SET /A ChatCurrentLine+=1
    :: If the line number is higher than the number of totally read lines, compute it
    IF !ChatCurrentLine! GTR %ChatLinesRead% (
        :: Login command
        IF "!ChatLine:~0,6!" == "/login" (
            IF "!ChatLine:~8!" == "" (
                SET /A ChatUsersOnline+=1    
            ) ELSE (
                SET /A ChatUsersOnline=!ChatLine:~8!
            )
            TITLE BatChat - Approx. !ChatUsersOnline! users online - Topic is "!ChatTopic!"
            SET ChatCommand=yes
        )
        
        :: Logout command
        IF "!ChatLine:~0,7!" == "/logout" (
            IF %ChatUsersOnline% GTR 0 (
                SET /A ChatUsersOnline-=1
            )
            TITLE BatChat - Approx. !ChatUsersOnline! users online - Topic is "!ChatTopic!"
            SET ChatCommand=yes
        )
        
        :: Topic command
        IF "!ChatLine:~0,7!" == "/topic " (
            SET ChatTopic=!ChatLine:~7!
            TITLE BatChat - Approx. %ChatUsersOnline% users online - Topic is "!ChatTopic!"
            SET ChatCommand=yes
        )
        
        
        :: Delete log command
        IF "!ChatLine:~0,5!" == "/cls " (
            DEL "%ChatLog%"
            ECHO /login #%ChatUsersOnline%>>"%ChatLog%
            ECHO %ChatHour%:%ChatMin%:%ChatSec%�!ChatLine:~5! cleared the chat log.>>"%ChatLog%"
            GOTO RereadLog
        )
        
        :: Color command
        IF "!ChatLine:~0,7!" == "/color " (
            COLOR !ChatLine:~7!
            SET ChatCommand=yes
        )
        
        :: Beep command
        IF "!ChatLine:~0,6!" == "/beep " (
            IF %ChatSuppressBeep% == yes (
                ECHO %ChatHour%:%ChatMin%:%ChatSec%�!ChatLine:~6! wants you to hear the beep.
            ) ELSE (
                ECHO %ChatHour%:%ChatMin%:%ChatSec%�!ChatLine:~6! wants you to hear the beep.
            )
            SET ChatCommand=yes
        )
        
        :: Other text
        IF NOT "!ChatCommand!" == "yes" (
            ECHO !ChatLine!
        )
    )
)
SET /A ChatLinesRead=%ChatCurrentLine%
SET ChatSuppressBeep=no

:Wait
IF NOT "%ChatRealtime%" == "yes" (
    PING 127.0.0.1 -n 2 > NUL
)
GOTO ReadLog


:ChatLogNotFound
ECHO ��������������������������������������������������������Ŀ
ECHO � CHATLOG NOT FOUND                                      �
ECHO ����������������������������������������������������������
ECHO � Log path � %ChatLog%
ECHO ������������
GOTO :Wait