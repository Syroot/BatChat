# BatChat

This is a very silly and old "network" capable application to chat with other people, written using Windows Batch file
scripts only. It supports features like /me, /anon, /topic (diplayed in console title), changing the text colors for
everyone, or sending around beeps (which will only play the default Windows notification sound nowadays).

- Start it using the `BatChat.bat` file. It'll ask you for a nick under which you'll send messages, and then opens a
  second window in which sent and received messages will appear.
- To actually make it network capable, open the `BatChat Config.bat` file in an editor and change the `ChatLog` variable
  to a path shared on a network drive mapped for all participants.
