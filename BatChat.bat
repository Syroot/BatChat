@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET ChatFirstLogin=yes
:Login
CLS
COLOR 0A
TITLE BatChat - Login
ECHO 浜様様様様様様様様様様様様様様様様様様様様様様様様様様様融
ECHO �  桎烝炳               桎烝炳                     樛樛  �
ECHO �  �    �   樛樛� 樛樛� �    � �   � 樛樛� 樛樛�      �  �
ECHO �  桀樛樂樛 �   �   �   桀     �   � �   �   �        �  �
ECHO �  栩     � 桀樛�   桀  栩     桀樛� 桀樛�   桀    樛樂  �
ECHO �  栩     � 栩  �   栩  栩   � 栩  � 栩  �   栩    �     �
ECHO �  栩樛樛樂 栩  �   栩  栩樛樂 栩  � 栩  �   栩    桀樛  �
ECHO �  憶� You just can't get any dumber chat than this 葦�  �
ECHO 麺様様様様様様様様様様様様様様様様様様様様様様様様様様様郵
ECHO � VERSION 2.0 (05.09.2011)                made by Syroot �
ECHO 藩様様様様様様様様様様様様様様様様様様様様様様様様様様様夕


ECHO 敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳朕
ECHO � CONFIGURATION                                          �
IF /I NOT EXIST "BatChat Config.bat" (
    ECHO 団陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳調
    ECHO � Configuration file not found.                          �
    ECHO � A new file has been created with the default settings: �
    ECHO ::Path to the file containing the chat log>>"BatChat Config.bat"
    ECHO SET ChatLog=chatlog.txt>>"BatChat Config.bat"
    ECHO.>>"BatChat Config.bat"
    ECHO ::Show received messages in realtime or with a pause of 1 second.>>"BatChat Config.bat"
    ECHO ::Warning: If enabled, this causes high CPU and traffic load!>>"BatChat Config.bat"
    ECHO SET ChatRealtime=no>>"BatChat Config.bat"
    ECHO.>>"BatChat Config.bat"
    ECHO ::Show the machine name when logging in?>>"BatChat Config.bat"
    ECHO SET ChatMachine=yes>>"BatChat Config.bat"
)
CALL "BatChat Config.bat"
ECHO 団陳陳陳陳陳陳陳陳陳陳陳陳陳賃陳陳陳陳陳陳陳陳陳陳陳陳陳潰
ECHO � Path to chatlog file       � %ChatLog%
ECHO � Realtime chatting          � %ChatRealtime%
ECHO � Show machine name at login � %ChatMachine%
ECHO 団陳陳陳陳陳陳陳陳陳陳陳陳陳珍陳陳陳陳陳陳陳陳陳陳陳陳陳朕
ECHO � To change these settings, edit "BatChat Config.bat".   �
ECHO 青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳潰

:EnterNick
SET /P ChatUser="What's your name?: "
If "%ChatUser%" == "" (
    GOTO EnterNick
)
IF "%ChatUser%" == "/exit" (
    EXIT
)
IF /I NOT EXIST "BatChat In.bat" (
    ECHO 敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳朕
    ECHO � Batch for "Received Messages" window not found.        �
    ECHO � Make sure the batch is named "BatChat In.bat" or       �
    ECHO � reinstall Batchat. Continuing without this feature.    �
    ECHO 青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳潰
    PAUSE > NUL
) ELSE (
    IF %ChatFirstLogin% == yes (
        SET ChatFirstLogin=no
        START "BatChat - Received Messages" "BatChat In.bat"
    )
)
SET ChatHour=%time:~0,2%
SET ChatMin=%time:~3,2%
SET ChatSec=%time:~6,2%
ECHO /login>>"%ChatLog%"
IF "%ChatMachine%" == "yes" (
    ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% @ %COMPUTERNAME% joined the chat.>>"%ChatLog%"
) ELSE (
    ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% joined the chat.>>"%ChatLog%"
)
TITLE BatChat - Logged in as %ChatUser%

:EnterMsg
CLS
SET ChatMsg=
ECHO 敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳朕
ECHO � ENTER YOUR MESSAGE                                     �
ECHO 団陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳調
ECHO � Commands: /me /anon /topic /color /beep /nick          �
ECHO �           /logout /exit                                �
ECHO � Other messages starting with "/" will be DOS-executed. �
ECHO 青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳潰
SET ChatHour=%time:~0,2%
SET ChatMin=%time:~3,2%
SET ChatSec=%time:~6,2%
SET /P ChatMsg="%ChatHour%:%ChatMin%:%ChatSec% %ChatUser%> "

SET ChatHour=%time:~0,2%
SET ChatMin=%time:~3,2%
SET ChatSec=%time:~6,2%
IF "%ChatMsg%" == "" (
    GOTO EnterMsg
)
IF /I "%ChatMsg%" == "/cls" (
    ECHO /cls %ChatUser%>>"%ChatLog%"
    GOTO EnterMsg
)
IF /I "%ChatMsg%" == "/reset" (
    ECHO /login #1>>"%ChatLog%"
    ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% reset the online user count.>>"%ChatLog%"
    GOTO EnterMsg
)
IF /I "%ChatMsg:~0,4%" == "/me " (
    ECHO %ChatHour%:%ChatMin%:%ChatSec% %ChatUser% %ChatMsg:~4%>>"%ChatLog%"
    GOTO EnterMsg
)
IF /I "%ChatMsg:~0,6%" == "/anon " (
    ECHO %ChatHour%:%ChatMin%:%ChatSec% [%ChatMsg:~6%]>>"%ChatLog%"
    GOTO EnterMsg
)
IF /I "%ChatMsg:~0,7%" == "/topic " (
    IF NOT "%ChatMsg:~8%" == "" (
        ECHO /topic %ChatMsg:~7%>>"%ChatLog%"
        ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% changed the topic to "%ChatMsg:~7%".>>"%ChatLog%"
    )
    GOTO EnterMsg
)
IF /I "%ChatMsg:~0,7%" == "/color " (
    IF NOT "%ChatMsg:~8%" == "" (
        ECHO /color %ChatMsg:~7,2%>>"%ChatLog%"
        ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% changed the color to %ChatMsg:~7,2%.>>"%ChatLog%"
    )
    GOTO EnterMsg
)
IF /I "%ChatMsg:~0,5%" == "/beep" (
    ECHO /beep %ChatUser%>>"%ChatLog%"
    GOTO EnterMsg
)
IF /I "%ChatMsg:~0,6%" == "/nick " (
    IF NOT "%ChatMsg:~6" == "" (
        SET ChatUser=%ChatMsg:~6%
        ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% is now known as !ChatUser!.>>"%ChatLog%"
    )
    GOTO EnterMsg
)
IF /I "%ChatMsg:~0,7%" == "/logout" (
    ECHO /logout>>"%ChatLog%"
    IF "%ChatMsg:~8%" == "" (
        ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% left the chat.>>"%ChatLog%"
    ) ELSE (
        ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% left the chat with message: %ChatMsg:~8%>>"%ChatLog%"
    )
    GOTO Login
)
IF /I "%ChatMsg:~0,5%" == "/exit" (
    ECHO /logout>>"%ChatLog%"
    IF "%ChatMsg:~6%" == "" (
        ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% left the chat.>>"%ChatLog%"
    ) ELSE (
        ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% left the chat with message: %ChatMsg:~6%>>"%ChatLog%"
    )
    GOTO Exit
)
IF "%ChatMsg:~0,1%" == "/" (
    ECHO %ChatHour%:%ChatMin%:%ChatSec%�%ChatUser% executed %ChatMsg:~1%>>"%ChatLog%"
    CLS
    ECHO 敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳朕
    ECHO � COMMAND EXECUTION                                      �
    ECHO 団陳陳陳陳堕陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳潰
    ECHO � Command � %ChatMsg:~1%
    ECHO 青陳陳陳陳�
    @ECHO ON
    %ChatMsg:~1%
    @ECHO OFF
    ECHO 敖陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳朕
    ECHO � End of execution. Press any key to return to chat...   �
    ECHO 青陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳潰
    PAUSE > NUL
    GOTO EnterMsg
)
ECHO %ChatHour%:%ChatMin%:%ChatSec% %ChatUser%: %ChatMsg%>>"%ChatLog%"
GOTO EnterMsg


:Exit
COLOR
