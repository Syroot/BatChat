::Path to the file containing the chat log
SET ChatLog=chatlog.txt

::Show received messages in realtime or with a pause of 1 second.
::Warning: If enabled, this causes high CPU and traffic load
SET ChatRealtime=no

::Show the machine name when logging in?
SET ChatMachine=yes
